#!/bin/bash

set -ex

JUPYTER_TOKEN=1HeIWJsNfcIIBu5XyyRp
IMAGE_NAME=instituteforsoftware/workshoptage-2019
DEPLOY_IMAGE=instituteforsoftware/workshoptage-2019-ai-deployer

# should you need to update, uncomment the following lines to remove the containers
# prior to 
# {
#   docker rm -vf wstage-2019
# } | {
#   echo 'wstage-2019 not existent'
# }
# {
#   docker rm -vf wstage2019-deploy
# } | {
#   echo 'wstage2019-deploy not existent'
# }

docker pull ${IMAGE_NAME}
docker pull ${DEPLOY_IMAGE}

# if --shm-size 50G doesn't work, try it with
# --ipc="host"  which isn't recommended, 
# because it is more open and quite insecure

docker run \
  --gpus all \
  -v /home/ubuntu/workshoptage:/home/jovyan/ \
  -e JUPYTER_TOKEN=${JUPYTER_TOKEN} \
  --name "wstage-2019" \
  --user="1000:1000" \
  -p "8888:8888" \
  --shm-size 50G \
  --restart "always" \
  -d \
  "${IMAGE_NAME}"

docker run \
  -v /home/ubuntu/workshoptage/exported_models:/models \
  -e MODELS_PATH=/models/ \
  -e MODEL_NAME=export.pkl \
  --name "wstage2019-deploy" \
  -p "5000:5000" \
  --restart "always" \
  -d \
  "${DEPLOY_IMAGE}"