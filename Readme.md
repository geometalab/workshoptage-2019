# Repo für Spatial DataScience und ML -  Workshoptage 2019

Dieses Repository dient als Grundlage für den Workshop 2019.

Dort verwenden wir vorbereitete Instanzen mit GPU
von https://salamander.ai/. Dies ist nicht zwingend nötig,
ein beliebiger Laptop/Server mit GPU und Linux (Ubuntu 18.04)
wird benötigt mit folgenden Sachen drauf:

* docker (https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-engine---community)
* nvidia-docker-runtime (https://github.com/NVIDIA/nvidia-docker)
* cuda Treiber (https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=1804&target_type=debnetwork)

und den entsprechenden Images nud commands.

Dies ist unter `setup/install_prerequisites.sh` zu finden.

Starten der container kann mit `setup/pull_and_run_notebook.sh` erreicht werden.

Dockerfile für einen eigenen build befindet sich im Ordner `setup/docker-image`

Hinweis: docker-compose ist momentan nicht kompatibel mit GPU; das kann bald ändern oder
noch so bleiben, falls dies jemand in der Zukunft liest: gerne Pull-Request für Anleitung.
